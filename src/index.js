const express = require('express');
const path = require('path');
const hbs = require("hbs");
const getGeoCode = require("./utils/getGeoCode");
const forecast = require("./utils/forecast");
const app = express();


const publicDir = path.join(__dirname, "../public");
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, "../templates/partials");


app.set("view engine", "hbs");
app.set("views", viewsPath);
hbs.registerPartials(partialsPath)
app.use(express.static(publicDir))

app.get("/", (req, res) => {
    res.render('index', {
        title: "Weather app",
    });
})

app.get("/about", (req, res) => {
    res.render('about', {
        name: "Dharmil"
    });
})

app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.send({
            error: "you must provide an address"
        })
    }

    getGeoCode(req.query.address, (err, { latitude, longitude, location } = {}) => {
        if (err) {
            return res.send({ err })
        }

        forecast(latitude, longitude, (err, forecastData) => {
            if (err) {
                return res.send({ err })
            }
            res.send({
                forecast: forecastData,
                location,
                address: req.query.address
            })
        })
    })
})


app.get("*", (req, res) => {
    res.send("Error 404 page")
})

app.listen(3000, (err) => {
    if (err) throw new err;
    console.log("Server Created on 3000");
});