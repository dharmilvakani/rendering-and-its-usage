const req = require('postman-request');

const forecast = (latitude, longitude, callback) => {
    const url = "http://api.weatherstack.com/current?access_key=f89b529b3144284ca6ce5cfb6f6c6028&query=" + latitude + "," + longitude;
    req({ url: url }, (err, res) => {
        const data = JSON.parse(res.body);
        if (err) {
            console.log("Unable to connect the server");
        }
        else if (data.error) {
            console.log(data.error);
        }
        else {
            callback(undefined, `${data.current.weather_descriptions[0]}.It is currently ${data.current.temperature} degrees out.There is a ${data.current.cloudcover}% chance of rain.`)
        }
    })
}

module.exports = forecast