const req = require('postman-request')

const getGeoCode = (address, callback) => {
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + encodeURIComponent(address) + '.json?access_token=pk.eyJ1IjoiZGhhcm1pbHZha2FuaSIsImEiOiJja3pjZ2U5MnUwY2IzMnhyc2sxeTh6OTRwIn0.qv-KIExUc2wpaA5lS51TaQ';

    req({ url, json: true }, (err, res) => {
        const data = res.body
        if (err) {
            console.log("Looks like you are not connected to the internet");
        }
        else if (data.features.length === 0) {
            console.log("Sorry we couldn't find the location");
        }
        else {
            callback(undefined, {
                longitude: data.features[0].center[0],
                latitude: data.features[0].center[1],
                location: data.features[0].place_name
            })
        }
    })
}

module.exports = getGeoCode